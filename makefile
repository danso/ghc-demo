SHELL = /bin/sh

HSFLAGS := -Wall -Wextra -dynamic -O1
GHC_FLAGS := -hidir cache/ -odir cache/ -isrc/

.PHONY: all
all: bin/hello bin/greetings

bin cache: 
	mkdir $@

bin/greetings: src/greetings.hs src/Common.hs | bin cache
	ghc $(HSFLAGS) $(GHC_FLAGS) -o $@ $<

bin/hello: src/hello.hs src/Common.hs | bin cache
	ghc $(HSFLAGS) $(GHC_FLAGS) -o $@ $<


.PHONY: clean
clean:
	rm bin/* cache/*
