# ghc-demo

this repository demonstrates two odd behaviours of GHC:

* `cache/Main.hi` and `cache/Main.o` are created,
    despite pragmas at the top of their respective source files;

* `cache/Main.hi` and `cache/Main.o` are re-used
    when compiling both binaries.

build the binaries with `make`.
then try running `bin/hello` and `bin/greetings`.
one of them might surprise you!

tested with GHC 9.0.2

# project structure

```
.
└── src/
    ├── Common.hs
    ├── greetings.hs
    └── hello.hs
```

`greetings.hs` and `hello.hs` both use values exported from the `Common` module.

running `make` compiles all three files, storing the `.hi` and `.o` files in `cache/` and the executables in `bin/`.
